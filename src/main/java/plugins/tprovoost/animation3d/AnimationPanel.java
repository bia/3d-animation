package plugins.tprovoost.animation3d;

import icy.gui.component.button.IcyButton;
import icy.main.Icy;
import icy.preferences.XMLPreferences;
import icy.preferences.XMLPreferences.XMLPreferencesRoot;
import icy.resource.icon.IcyIcon;
import icy.util.XMLUtil;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;
import javax.swing.event.ChangeListener;
import javax.swing.filechooser.FileNameExtensionFilter;

import org.w3c.dom.Element;

import plugins.tprovoost.flycam.CameraPosition;

public class AnimationPanel extends JPanel
{
    private static final long serialVersionUID = 1L;

    private static final String PREF_CAMERAS = "cameras";
    private static final String PREF_CAM_LOCATION_DATA = "CamLocationData";

    private static final String PREF_FPS = "fps";
    private static final String PREF_TIME = "time";
    private static final String PREF_SMOOTH = "smooth";

    private KeySlider timeSlider;
    private IcyButton gotostart;
    private IcyButton gotoend;
    private JButton playbackward;
    private IcyButton playforward;
    private IcyButton stop;
    private JButton previousframe;
    private JButton nextframe;
    private JButton record;
    private JButton recordCancel;
    private JButton previouskey;
    private JButton nextkey;
    private JButton addkey;
    private JButton removekey;
    private JButton resetkey;
    private JButton rescale;
    private JTextField rescalefps;
    private JTextField rescaletime;
    private JTextField smoothpathfactor;
    private JProgressBar recordProgressBar;
    private JCheckBox checkBoxAutoUpdate;
    private JButton openanimation;
    private JButton saveanimation;
    private ActionListener actionListener;
    private ChangeListener changeListener;
    private JComboBox comboPresets;
    private JCheckBox cboxAutoChangeSeqT;
    private JPanel panelLoadSave;
    private JButton btnLoad;
    private JButton btnSave;

    private enum AnimationPreset
    {
        CUSTOM
        {
            @Override
            public String toString()
            {
                return "Custom";
            }
        },
        ROTATION_X
        {
            @Override
            public String toString()
            {
                return "Rotation on X";
            }
        },
        ROTATION_Y
        {
            @Override
            public String toString()
            {
                return "Rotation on Y";
            }
        },
        ROTATION_Z
        {
            @Override
            public String toString()
            {
                return "Rotation on Z";
            }
        },
        GO_INTO
        {
            @Override
            public String toString()
            {
                return "Go Into";
            }
        }
    };

    public AnimationPanel(ActionListener actionListener, ChangeListener changeListener)
    {
        this.actionListener = actionListener;
        this.changeListener = changeListener;
        this.setPreferredSize(new Dimension(200, 465));

        timeSlider = new KeySlider(0, 5 * 15, 15);
        timeSlider.setBorder(new EmptyBorder(4, 4, 4, 4));
        timeSlider.setSnapToTicks(false);
        timeSlider.setValue(0);
        timeSlider.addChangeListener(changeListener);
        timeSlider.setPreferredSize(new Dimension(290, 60));

        playbackward = new JButton("<");
        playbackward.setActionCommand("playbackward");
        playbackward.addActionListener(actionListener);

        record = new JButton("Render");
        record.setActionCommand("record");
        record.addActionListener(actionListener);

        recordCancel = new JButton("Cancel");
        // recordCancel.setEnabled(false);
        recordCancel.setActionCommand("recordCancel");
        recordCancel.addActionListener(actionListener);

        recordProgressBar = new JProgressBar();
        recordProgressBar.setMaximum(100);
        recordProgressBar.setValue(0);

        openanimation = new JButton("Open...");
        openanimation.setActionCommand("openanimation");
        openanimation.addActionListener(actionListener);

        saveanimation = new JButton("Save...");
        saveanimation.setActionCommand("saveanimation");
        saveanimation.addActionListener(actionListener);

        JPanel rescalePanel = new JPanel();
        rescalePanel.setPreferredSize(new Dimension(300, 80));
        rescalePanel.setBorder(BorderFactory.createTitledBorder("Rescale tool"));
        rescalePanel.setLayout(new BoxLayout(rescalePanel, BoxLayout.Y_AXIS));

        JPanel panel_1 = new JPanel();
        panel_1.setBorder(new EmptyBorder(4, 4, 4, 4));
        rescalePanel.add(panel_1);
        panel_1.setLayout(new GridLayout(0, 2, 0, 0));

        JLabel label = new JLabel("FPS:");
        panel_1.add(label);

        rescalefps = new JTextField();
        panel_1.add(rescalefps);
        rescalefps.setPreferredSize(new Dimension(30, 20));
        rescalefps.setText("" + getTimeSlider().getFps());

        JPanel panel_2 = new JPanel();
        panel_2.setBorder(new EmptyBorder(4, 4, 4, 4));
        rescalePanel.add(panel_2);
        panel_2.setLayout(new GridLayout(0, 2, 0, 0));
        JLabel label_1 = new JLabel("Seconds :");
        panel_2.add(label_1);
        rescaletime = new JTextField("5");
        panel_2.add(rescaletime);
        rescaletime.setPreferredSize(new Dimension(30, 20));

        JPanel panelRescaleButton = new JPanel();
        rescalePanel.add(panelRescaleButton);
        panelRescaleButton.setLayout(new BoxLayout(panelRescaleButton, BoxLayout.X_AXIS));

        panelRescaleButton.add(Box.createHorizontalGlue());

        rescale = new JButton("Rescale");
        panelRescaleButton.add(rescale);
        rescale.setActionCommand("rescale");

        panelRescaleButton.add(Box.createHorizontalGlue());
        rescale.addActionListener(actionListener);

        JPanel smoothingPanel = new JPanel();
        smoothingPanel.setBorder(BorderFactory.createTitledBorder("Smooth Camera path"));
        smoothingPanel.setLayout(new GridLayout(0, 2, 0, 0));
        smoothingPanel.add(new JLabel("Smooth factor: "));
        setSmoothpathfactor(new JTextField("0"));
        getSmoothpathfactor().setPreferredSize(new Dimension(190, 20));
        smoothingPanel.add(getSmoothpathfactor());

        JPanel p8 = new JPanel();
        p8.setBorder(BorderFactory.createTitledBorder("Record"));
        p8.setLayout(new GridLayout(4, 1));

        cboxAutoChangeSeqT = new JCheckBox("Auto Change Time Position");
        cboxAutoChangeSeqT.setHorizontalAlignment(SwingConstants.CENTER);
        cboxAutoChangeSeqT.setToolTipText("Also modifies the position of the current T in the Sequence.");
        p8.add(cboxAutoChangeSeqT);
        p8.add(record);
        p8.add(recordCancel);
        p8.add(getRecordProgressBar());
        setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));

        // JPanel p9 = new JPanel();
        // p9.setBorder( BorderFactory.createTitledBorder("File"));
        // p9.add( openanimation );
        // p9.add( saveanimation );

        this.add(getTimeSlider());

        comboPresets = new JComboBox();
        comboPresets.setModel(new DefaultComboBoxModel(AnimationPreset.values()));
        comboPresets.addItemListener(new ItemListener()
        {

            @Override
            public void itemStateChanged(ItemEvent e)
            {
                if (e.getStateChange() == ItemEvent.SELECTED)
                {
                    switch (AnimationPreset.valueOf(comboPresets.getSelectedItem().toString()))
                    {
                        case ROTATION_X:
                        {
                            // TODO
                        }
                            break;

                        case ROTATION_Y:
                        {
                            // TODO
                        }
                            break;

                        case ROTATION_Z:
                        {
                            // TODO
                        }
                            break;

                        case CUSTOM:
                        default:
                            break;
                    }
                }

            }
        });

        panelLoadSave = new JPanel();
        add(panelLoadSave);
        panelLoadSave.setLayout(new GridLayout(1, 0, 0, 0));

        btnLoad = new JButton("Load");
        btnLoad.addActionListener(new ActionListener()
        {

            @Override
            public void actionPerformed(ActionEvent e)
            {
                String filename = animFileChooser();
                if (filename == null)
                    return;
                XMLPreferencesRoot root = new XMLPreferencesRoot(filename);
                loadXMLFile(root.getPreferences());
            }
        });
        panelLoadSave.add(btnLoad);

        btnSave = new JButton("Save");
        btnSave.addActionListener(new ActionListener()
        {

            @Override
            public void actionPerformed(ActionEvent e)
            {
                String filename = animFileChooser();
                if (filename == null)
                    return;
                XMLPreferencesRoot root = new XMLPreferencesRoot(filename);
                saveToXML(root.getPreferences());
                root.save();
            }
        });
        panelLoadSave.add(btnSave);
        // add(comboPresets); // TODO

        JPanel panel = new JPanel();
        add(panel);

        playforward = new IcyButton(new IcyIcon("sq_next.png"));
        playforward.setActionCommand("playforward");
        playforward.addActionListener(actionListener);

        previousframe = new IcyButton(new IcyIcon("sq_br_prev.png"));
        previousframe.setActionCommand("previousframe");
        previousframe.addActionListener(actionListener);
        panel.setLayout(new GridLayout(0, 2, 0, 0));

        JPanel p1 = new JPanel();
        p1.setBorder(new TitledBorder(null, "Frames", TitledBorder.LEADING, TitledBorder.TOP, null, null));
        panel.add(p1);
        p1.setPreferredSize(new Dimension(200, 50));
        p1.setLayout(new GridLayout(2, 3));
        p1.add(previousframe);
        p1.add(getPlayforward());

        gotostart = new IcyButton(new IcyIcon("sq_br_first.png"));
        gotostart.setActionCommand("gotostart");
        gotostart.addActionListener(actionListener);

        nextframe = new IcyButton(new IcyIcon("sq_br_next.png"));
        nextframe.setActionCommand("nextframe");
        nextframe.addActionListener(actionListener);
        p1.add(nextframe);

        p1.add(gotostart);

        stop = new IcyButton(new IcyIcon("square_shape.png"));
        stop.setActionCommand("stop");
        stop.addActionListener(actionListener);
        p1.add(stop);

        gotoend = new IcyButton(new IcyIcon("sq_br_last.png"));
        gotoend.setActionCommand("gotoend");
        gotoend.addActionListener(actionListener);
        p1.add(gotoend);

        previouskey = new IcyButton(new IcyIcon("arrow_left.png"));
        previouskey.setActionCommand("previouskey");
        previouskey.addActionListener(actionListener);

        nextkey = new IcyButton(new IcyIcon("arrow_right.png"));
        nextkey.setActionCommand("nextkey");
        nextkey.addActionListener(actionListener);

        addkey = new IcyButton(new IcyIcon("pin_map.png"));
        addkey.setActionCommand("addkey");
        addkey.addActionListener(actionListener);

        removekey = new IcyButton(new IcyIcon("delete.png"));
        removekey.setActionCommand("removekey");
        removekey.addActionListener(actionListener);

        resetkey = new IcyButton(new IcyIcon("playback_reload.png"));
        resetkey.setActionCommand("resetkey");
        resetkey.addActionListener(actionListener);

        JPanel keyPanel = new JPanel();
        panel.add(keyPanel);
        keyPanel.setLayout(new GridLayout(2, 3));
        keyPanel.setBorder(BorderFactory.createTitledBorder("Keys"));
        keyPanel.setPreferredSize(new Dimension(300, 75));

        keyPanel.add(addkey);
        keyPanel.add(previouskey);
        keyPanel.add(nextkey);
        keyPanel.add(removekey);
        keyPanel.add(resetkey);

        checkBoxAutoUpdate = new JCheckBox("Auto Update");
        // panel.add(checkBoxAutoUpdate);
        checkBoxAutoUpdate.setSelected(true);

        this.add(rescalePanel);
        this.add(smoothingPanel);
        this.add(p8);
        // this.add(p9);
    }

    public void removeListeners()
    {
        timeSlider.removeChangeListener(changeListener);
        gotostart.removeActionListener(actionListener);
        gotoend.removeActionListener(actionListener);
        playbackward.removeActionListener(actionListener);
        playforward.removeActionListener(actionListener);
        stop.removeActionListener(actionListener);
        previousframe.removeActionListener(actionListener);
        nextframe.removeActionListener(actionListener);
        record.removeActionListener(actionListener);
        recordCancel.removeActionListener(actionListener);
        previouskey.removeActionListener(actionListener);
        nextkey.removeActionListener(actionListener);
        addkey.removeActionListener(actionListener);
        removekey.removeActionListener(actionListener);
        resetkey.removeActionListener(actionListener);
        rescale.removeActionListener(actionListener);
        rescalefps.removeActionListener(actionListener);
        rescaletime.removeActionListener(actionListener);
        smoothpathfactor.removeActionListener(actionListener);
        checkBoxAutoUpdate.removeActionListener(actionListener);
        openanimation.removeActionListener(actionListener);
        saveanimation.removeActionListener(actionListener);
        actionListener = null;
        changeListener = null;
        timeSlider = null;
    }

    public void setEnabledRecursive(JPanel parent, boolean enabled)
    {
        parent.setEnabled(enabled);
        Component[] components = parent.getComponents();
        if (components != null && components.length > 0)
        {
            int count = components.length;
            for (int i = 0; i < count; i++)
            {
                components[i].setEnabled(enabled);
                if (components[i].getClass().getName() == "javax.swing.JPanel")
                {
                    setEnabledRecursive((JPanel) components[i], enabled);
                }
            }
        }
    }

    /**
     * @return the timeSlider
     */
    public KeySlider getTimeSlider()
    {
        return timeSlider;
    }

    /**
     * @param timeSlider
     *        the timeSlider to set
     */
    public void setTimeSlider(KeySlider timeSlider)
    {
        this.timeSlider = timeSlider;
    }

    /**
     * @return the stop
     */
    public JButton getStop()
    {
        return stop;
    }

    /**
     * @param stop
     *        the stop to set
     */
    public void setStop(IcyButton stop)
    {
        this.stop = stop;
    }

    /**
     * @return the smoothpathfactor
     */
    public JTextField getSmoothpathfactor()
    {
        return smoothpathfactor;
    }

    /**
     * @param smoothpathfactor
     *        the smoothpathfactor to set
     */
    public void setSmoothpathfactor(JTextField smoothpathfactor)
    {
        this.smoothpathfactor = smoothpathfactor;
    }

    /**
     * @return the recordProgressBar
     */
    public JProgressBar getRecordProgressBar()
    {
        return recordProgressBar;
    }

    /**
     * @param recordProgressBar
     *        the recordProgressBar to set
     */
    public void setRecordProgressBar(JProgressBar recordProgressBar)
    {
        this.recordProgressBar = recordProgressBar;
    }

    /**
     * @return the playforward
     */
    public JButton getPlayforward()
    {
        return playforward;
    }

    /**
     * @param playforward
     *        the playforward to set
     */
    public void setPlayforward(IcyButton playforward)
    {
        this.playforward = playforward;
    }

    /**
     * @return the rescalefps
     */
    public JTextField getRescalefps()
    {
        return rescalefps;
    }

    /**
     * @param rescalefps
     *        the rescalefps to set
     */
    public void setRescalefps(JTextField rescalefps)
    {
        this.rescalefps = rescalefps;
    }

    /**
     * @return the rescaletime
     */
    public JTextField getRescaletime()
    {
        return rescaletime;
    }

    /**
     * @param rescaletime
     *        the rescaletime to set
     */
    public void setRescaletime(JTextField rescaletime)
    {
        this.rescaletime = rescaletime;
    }

    /**
     * @return the checkBoxAutoUpdate
     */
    public JCheckBox getCheckBoxAutoUpdate()
    {
        return checkBoxAutoUpdate;
    }

    /**
     * @param checkBoxAutoUpdate
     *        the checkBoxAutoUpdate to set
     */
    public void setCheckBoxAutoUpdate(JCheckBox checkBoxAutoUpdate)
    {
        this.checkBoxAutoUpdate = checkBoxAutoUpdate;
    }

    public JButton getCancel()
    {
        return recordCancel;
    }

    public boolean isAutoChangeSeqT()
    {
        return cboxAutoChangeSeqT.isSelected();
    }

    /**
     * Used to load XML Files
     * 
     * @return Returns null if Dialog exited.
     */
    private String animFileChooser()
    {
        String filename = null;
        JFileChooser fc = new JFileChooser();
        fc.setFileFilter(new FileNameExtensionFilter("Animation Files (.anim)", "anim"));
        int returnVal = fc.showDialog(Icy.getMainInterface().getMainFrame(), "Save File");
        if (returnVal == JFileChooser.APPROVE_OPTION)
        {
            filename = fc.getSelectedFile().getAbsolutePath();
            if (!filename.endsWith(".anim"))
                filename += ".anim";
        }
        else
        {
            filename = null;
        }
        
        return filename;
    }

    /**
     * Save all the properties into an XML file.
     * 
     * @param root
     *        : file and node where data is saved.
     */
    private void saveToXML(XMLPreferences root)
    {
        root.removeChildren();

        XMLPreferences cameras = root.node(PREF_CAMERAS);
        cameras.removeChildren();

        root.put(PREF_FPS, rescalefps.getText());
        root.put(PREF_TIME, rescaletime.getText());
        root.put(PREF_SMOOTH, smoothpathfactor.getText());

        // save camera positions into XML
        for (CameraPosition cam : timeSlider.getCameraPositions())
            cam.saveCamera(XMLUtil.addElement(cameras.getXMLNode(), PREF_CAM_LOCATION_DATA));
    }

    /**
     * Load all the properties into a file.
     * 
     * @param root
     *        : file and node where data is saved.
     */
    private void loadXMLFile(XMLPreferences root)
    {
        if (!root.nodeExists(PREF_CAMERAS))
            return;

        rescalefps.setText(root.get(PREF_FPS, "15"));
        rescaletime.setText(root.get(PREF_TIME, "5"));
        smoothpathfactor.setText(root.get(PREF_SMOOTH, "0"));
        XMLPreferences cameras = root.node(PREF_CAMERAS);

        List<CameraPosition> cams = timeSlider.getCameraPositions();
        cams.clear();
        for (Element e : XMLUtil.getElements(cameras.getXMLNode(), PREF_CAM_LOCATION_DATA))
        {
            CameraPosition cam = new CameraPosition();
            cam.loadCamera(e);
            cams.add(cam);
        }

        timeSlider.repaint();
    }
}
