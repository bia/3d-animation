package plugins.tprovoost.animation3d;

import java.awt.Color;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Insets;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JSlider;

import plugins.tprovoost.flycam.CameraPosition;

public class KeySlider extends JSlider implements MouseListener, MouseMotionListener
{
    private static final long serialVersionUID = 1L;
    // private ArrayList<Integer> key = new ArrayList<Integer>();
    private int fps = 15;
    private int timelength;
    private Point currentPoint = null;
    private CameraPosition currentCamera;
    private List<CameraPosition> cameraPositions = new ArrayList<CameraPosition>();

    public KeySlider(int min, int max, int value)
    {
        super(min, max, value);
        setPaintTicks(true);
        setPaintLabels(true);
        addMouseListener(this);
        addMouseMotionListener(this);
    }

    @Override
    protected void paintComponent(Graphics g)
    {
        super.paintComponent(g);
        Insets ins = getInsets();
        int w = getWidth();
        int h = getHeight();
        Graphics2D g2 = (Graphics2D) g.create();
        // g2.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING,
        // RenderingHints.VALUE_ANTIALIAS_ON);
        // g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

        // display red keys in component.
        g2.setColor(new Color(210, 27, 58));
        for (CameraPosition cam : cameraPositions)
        {
            float step = ((w - 31) / (float) getMaximum());
            int positionx = Math.round(ins.left + 11 + cam.timeIndex * step);
            g2.fillRect(positionx - 1, h - 12, 3, 6);
        }
        // // display Seconds:Frame.
        g2.setColor(Color.black);
        drawCenteredString(g2, "Frame: #" + getValue() + " / " + timeFormat(getValue()) + " s", getMaximum() / 2, 10,
                false);
        g2.dispose();
    }

    private String timeFormat(int t)
    {
        String modulo;
        if (t % getFps() < 10)
        {
            modulo = "0" + t % getFps();
        }
        else
        {
            modulo = "" + t % getFps();
        }
        String SecondFrame;
        SecondFrame = "" + (t / getFps()) + ":" + modulo + "0";
        return SecondFrame;
    }

    /**
     * @param g
     * @param s
     * @param v
     *        Tick corresponding to the center of the string.
     */
    private void drawCenteredString(Graphics g, String s, int v, int y, boolean ShowTick)
    {
        FontMetrics metrics = getFontMetrics(g.getFont());
        float step = ((getWidth() - 16.0f) / (float) getMaximum());
        int positionx = Math.round(6 + v * step);
        g.drawString(s, positionx - metrics.stringWidth(s) / 2, y);
        if (ShowTick)
            g.drawLine(positionx, 35, positionx, 30);
    }

    // public void addKey(int timeKey)
    // {
    // key.add(timeKey);
    // repaint();
    // }
    //
    // public void removeKey(int TimeKey)
    // {
    // key.remove((Integer) TimeKey);
    // repaint();
    // }
    //
    // public void removeAllKey()
    // {
    // key.clear();
    // repaint();
    // }

    // public ArrayList<Integer> getKeys()
    // {
    // return key;
    // }

    /**
     * @return the timelength
     */
    public int getTimelength()
    {
        return timelength;
    }

    /**
     * @param timelength
     *        the timelength to set
     */
    public void setTimelength(int timelength)
    {
        this.timelength = timelength;
    }

    /**
     * @return the fps
     */
    public int getFps()
    {
        return fps;
    }

    /**
     * @param fps
     *        the fps to set
     */
    public void setFps(int fps)
    {
        this.fps = fps;
    }

    @Override
    public void setMaximum(int maximum)
    {
        super.setMaximum(maximum);
        if (maximum < 5)
        {
            setMinorTickSpacing(maximum);
            setLabelTable(createStandardLabels(maximum));
        }
        else
        {
            setMajorTickSpacing(maximum / 5);
            setMinorTickSpacing(maximum / 10);
            setLabelTable(createStandardLabels(maximum / 5));
        }
    }

    @Override
    public void mouseDragged(MouseEvent e)
    {
        if (currentPoint != null && currentCamera != null)
        {
            int xmov = e.getPoint().x - currentPoint.x;
            int max = getMaximum();
            float step = (getWidth()) / (float) max;
            int timeIndex = (int) (e.getPoint().x / step);

            if (timeIndex == 0)
            {
                timeIndex = 1;
            }
            else if (timeIndex >= max)
            {
                timeIndex = max;
            }
            else if (cameraExists(timeIndex))
            {
                if (Math.signum(xmov) == -1.0d)
                {
                    timeIndex--;
                }
                else if (Math.signum(xmov) == 1.0d)
                {
                    timeIndex++;
                }
            }
            System.out.println(timeIndex);
            currentCamera.timeIndex = timeIndex;
            currentPoint = e.getPoint();
        }
        repaint();
    }

    private boolean cameraExists(int timeIndex)
    {
        for (CameraPosition cam : cameraPositions)
        {
            if (cam.timeIndex == timeIndex)
                return true;
        }
        return false;
    }

    @Override
    public void mouseMoved(MouseEvent e)
    {
    }

    @Override
    public void mouseClicked(MouseEvent e)
    {
    }

    @Override
    public void mousePressed(MouseEvent e)
    {
        Insets ins = getInsets();
        int w = getWidth();
        int h = getHeight();
        for (CameraPosition cam : cameraPositions)
        {
            float step = ((w - 31) / (float) getMaximum());
            int positionx = Math.round(ins.left + 11 + cam.timeIndex * step);
            Rectangle rect = new Rectangle(positionx - 1, h - 12, 3, 6);
            if (rect.contains(e.getPoint()))
            {
                currentCamera = cam;
                currentPoint = e.getPoint();
                break;
            }
        }
    }

    @Override
    public void mouseReleased(MouseEvent e)
    {
        currentCamera = null;
        currentPoint = e.getPoint();
    }

    @Override
    public void mouseEntered(MouseEvent e)
    {
    }

    @Override
    public void mouseExited(MouseEvent e)
    {
    }

    public void setCameraPositions(List<CameraPosition> value)
    {
        cameraPositions = value;
    }

    public List<CameraPosition> getCameraPositions()
    {
        return cameraPositions;
    }
}
