package plugins.tprovoost.animation3d;

import java.awt.Component;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.util.Hashtable;

import javax.swing.JPanel;

public class TablePanel extends JPanel
{

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    private Hashtable<String, Boolean> busy = new Hashtable<String, Boolean>();

    private GridBagLayout gridbag;
    private GridBagConstraints c;

    public final static int WEST = GridBagConstraints.WEST;
    public final static int EAST = GridBagConstraints.EAST;
    public final static int CENTER = GridBagConstraints.CENTER;

    private final Insets DEFAULT_INSETS = new Insets(2, 2, 2, 2);
    private final int DEFAULT_ANCHOR = CENTER;

    public TablePanel()
    {
        super();
        gridbag = new GridBagLayout();
        setLayout(gridbag);
        c = new GridBagConstraints();
        c.fill = GridBagConstraints.BOTH;
        // c.fill = GridBagConstraints.NONE;

        c.gridx = -1;
        c.gridy = -1;
        c.gridwidth = 1;
        c.gridheight = 1;

        c.weightx = 0.0; // see later
        c.weighty = 0.0; // see later

    }

    public void newLine()
    {
        c.gridx = -1;
        c.gridy++;
    }

    public Component add(Component component)
    {
        return add(component, 1, 1, null, -1);
    }

    public Component add(Component component, int colspan, int rowspan)
    {
        return add(component, colspan, rowspan, null, -1);
    }

    public Component add(Component component, int colspan, int rowspan, int anchor)
    {
        return add(component, colspan, rowspan, null, anchor);
    }

    public Component add(Component component, int colspan, int rowspan, Insets insets, int anchor)
    {
        c.gridx++;
        while (isBusy(c.gridx, c.gridy))
            c.gridx++;
        c.gridwidth = colspan;
        c.gridheight = rowspan;
        for (int i = 0; i < colspan; i++)
        {
            for (int j = 0; j < rowspan; j++)
            {
                setBusy(c.gridx + i, c.gridy + j);
            }
        }
        if (anchor != -1)
        {
            c.anchor = anchor;
        }
        else
        {
            c.anchor = DEFAULT_ANCHOR;
        }
        if (insets != null)
        {
            c.insets = insets;
        }
        else
        {
            c.insets = DEFAULT_INSETS;
        }

        gridbag.setConstraints(component, c);
        return super.add(component);
    }

    private void setBusy(int x, int y)
    {
        busy.put(new String(y + "-" + x), new Boolean(true));
    }

    private boolean isBusy(int x, int y)
    {
        return busy.get(new String(c.gridy + "-" + c.gridx)) != null;
    }
}
