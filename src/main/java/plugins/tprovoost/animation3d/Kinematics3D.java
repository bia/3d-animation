package plugins.tprovoost.animation3d;

// Management of generality
import icy.file.FileUtil;
import icy.gui.dialog.MessageDialog;
import icy.gui.frame.IcyFrame;
import icy.image.ImageUtil;
import icy.main.Icy;
import icy.preferences.PluginsPreferences;
import icy.preferences.XMLPreferences;
import icy.system.thread.ThreadUtil;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.swing.JFileChooser;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.filechooser.FileNameExtensionFilter;

import plugins.kernel.canvas.VtkCanvas;
import plugins.tprovoost.flycam.CameraPosition;
import plugins.tprovoost.flycam.FlyCamOverlay;

import com.xuggle.mediatool.IMediaWriter;
import com.xuggle.mediatool.ToolFactory;
import com.xuggle.xuggler.ICodec;
import com.xuggle.xuggler.IPixelFormat;
import com.xuggle.xuggler.IRational;
import com.xuggle.xuggler.IStreamCoder;

public class Kinematics3D extends IcyFrame
{
    private static final String LAST_DIR = "last_directory";
    private XMLPreferences prefs = PluginsPreferences.root(Animation3D.class);

    /**
     * Original animation user's key.
     */
    private List<CameraPosition> cameraPositions;
    private AnimationPanel animPanel = null;
    private PanelKinematics kinematicPanel = null;
    private FlyCamOverlay flyCam;
    private KinematicsThread kinemAnim;

    public Kinematics3D(FlyCamOverlay flyCam)
    {
        super("Animation 3D", true);

        cameraPositions = new ArrayList<CameraPosition>();
        kinematicPanel = new PanelKinematics(this);
        this.flyCam = flyCam;

        rescaleTimeLine();
        setContentPane(getPanel());
        pack();
    }

    // private float[] linearInterp(float[] v1, float[] v2, float h)
    // {
    // float[] toBeReturn = new float[3];
    // toBeReturn[0] = v1[0] * (1 - h) + v2[0] * h;
    // toBeReturn[1] = v1[1] * (1 - h) + v2[1] * h;
    // toBeReturn[2] = v1[2] * (1 - h) + v2[2] * h;
    // return toBeReturn;
    //
    // }

    public List<CameraPosition> getCameraPositions()
    {
        return cameraPositions;
    }

    public void setCameraPositions(List<CameraPosition> value)
    {
        if (value != null)
            cameraPositions = value;
    }

    public void seekAndDestroyKeyAt(int t)
    {
        synchronized (cameraPositions)
        {
            for (int i = cameraPositions.size() - 1; i >= 0; i--)
            {
                if (cameraPositions.get(i).timeIndex == t)
                    cameraPositions.remove(i);
            }
        }
    }

    public int getNumberOfUserKey()
    {
        return getCameraPositions().size();
    }

    /**
     * @return the first key available.
     * 
     * @author Fab
     */
    public CameraPosition getFirstCamKey()
    {
        if (getCameraPositions().isEmpty())
        {
            System.out.println("Kinematic3D : Problem : GetFirstCamKey should be able to get a valid key.");
            return null;
        }

        return getCameraPositions().get(0);
    }

    /**
     * @return the last key available.
     * 
     * @author Fab
     */
    public CameraPosition getLastCamKey()
    {
        if (getCameraPositions().isEmpty())
        {
            System.out.println("Kinematic3D : Problem : GetLastCamKey should be able to get a valid key.");
            return null;
        }
        return getCameraPositions().get(getNumberOfUserKey() - 1);
    }

    /**
     * @param t int
     * @param CreateKeyIfDoNotExists boolean
     * @return Get Previous Key &lt;=t.
     * 
     * @author Fab
     */
    public CameraPosition getPreviousFlyCamKey(int t, boolean CreateKeyIfDoNotExists)
    {
        if (t < 0)
            t = 0;

        CameraPosition Previous = null; // Previous detected key.
        int previousbest = animPanel.getTimeSlider().getMaximum();

        // find previous
        for (CameraPosition camPos : getCameraPositions())
        {
            // if (povk.CameraType == canvas.getCameraType())
            if (camPos.timeIndex <= t)
            {
                if ((t - camPos.timeIndex) < previousbest)
                {
                    previousbest = t - camPos.timeIndex;
                    Previous = camPos;
                }
            }
        }

        if (Previous == null && CreateKeyIfDoNotExists == true)
            return getFirstCamKey();

        return Previous;
    }

    /**
     * @param t int
     * @param CreateKeyIfDoNotExists boolean
     * @return Get Previous Key &gt;=t.
     * 
     * @author Fab
     */
    public CameraPosition getNextFlyCamKey(int t, boolean CreateKeyIfDoNotExists)
    {
        if (t > animPanel.getTimeSlider().getMaximum())
            t = animPanel.getTimeSlider().getMaximum();

        CameraPosition Next = null; // Previous detected key.
        int nextbest = animPanel.getTimeSlider().getMaximum();

        // find best
        for (CameraPosition camPos : getCameraPositions())
        {
            // if (povk.CameraType == canvas.getCameraType())
            if (camPos.timeIndex >= t)
            {
                if ((camPos.timeIndex - t) < nextbest)
                {
                    nextbest = camPos.timeIndex - t;
                    Next = camPos;
                }
            }
        }

        if (Next == null && CreateKeyIfDoNotExists == true)
            return getLastCamKey();

        return Next;
    }

    /**
     * @param t int
     * @param smooth int
     * @return CameraPosition
     */
    public CameraPosition computeSmoothInterpolatedCamKey(int t, int smooth)
    {
        CameraPosition cumul = new CameraPosition();

        int iter = 0;
        for (int i = t - smooth; i <= t + smooth; i++)
        {
            iter++;
            CameraPosition tmppovd = computeInterpolatedCamKey(i);
            for (int j = 0; j < 3; j++)
            {
                cumul.position[j] += tmppovd.position[j];
                cumul.focal[j] += tmppovd.focal[j];
                cumul.viewUp[j] += tmppovd.viewUp[j];
            }
            cumul.viewAngle += tmppovd.viewAngle;
        }

        for (int j = 0; j < 3; j++)
        {
            cumul.position[j] /= iter;
            cumul.focal[j] /= iter;
            cumul.viewUp[j] /= iter;
        }
        cumul.viewAngle /= iter;

        return cumul;
    }

    /**
     * Creates an interpolated FlyCamKey.
     * 
     * @param t
     *        Time index in animation's sequence ( frame # )
     * @return Interpolated FlyCamKey as CamPointOfViewData
     * @author Fab 27/11/06
     */
    public CameraPosition computeInterpolatedCamKey(int t)
    {
        CameraPosition povd = new CameraPosition();
        CameraPosition step = new CameraPosition();
        CameraPosition Previous = getPreviousFlyCamKey(t, true);
        CameraPosition Next = getNextFlyCamKey(t, true);

        if (Previous.timeIndex == Next.timeIndex)
            return Previous; // We are on the key. No need to interpolate.

        final int delta = Next.timeIndex - Previous.timeIndex;
        final int delta2 = t - Previous.timeIndex;

        for (int i = 0; i < 3; i++)
        {
            step.position[i] = (Next.position[i] - Previous.position[i]) / delta;
            step.focal[i] = (Next.focal[i] - Previous.focal[i]) / delta;
            step.viewUp[i] = (Next.viewUp[i] - Previous.viewUp[i]) / delta;
            povd.position[i] = Previous.position[i] + step.position[i] * delta2;
            povd.focal[i] = Previous.focal[i] + step.focal[i] * delta2;
            povd.viewUp[i] = Previous.viewUp[i] + step.viewUp[i] * delta2;
        }
        step.viewAngle = (Next.viewAngle - Previous.viewAngle) / delta;
        povd.viewAngle = Previous.viewAngle + step.viewAngle * delta2;

        return povd;
    }

    /**
     * @return Get the kinematic interaction panel
     */
    public JPanel getPanel()
    {
        return kinematicPanel.getPanel();
    }

    /**
     * needed when extends Object3D
     */
    public void display()
    {
        // nothing to do here

    }

    /**
     * clean up the object
     */
    public void unload()
    {

    }

    public void refreshAnimPanel()
    {
        // Put new computed key in Animation's Slider.
        // animPanel.getTimeSlider().removeAllKey();
        for (CameraPosition camPos : getCameraPositions())
        {
            // CameraPosition povk = e.nextElement();
            // if (canvas.getCameraType() == povk.CameraType)
            // animPanel.getTimeSlider().addKey(povk.timeIndex);

            // Pour forcer un rafraichissement.
            int i = animPanel.getTimeSlider().getValue();
            animPanel.getTimeSlider().setValue(i++);
            animPanel.getTimeSlider().setValue(i);

        }
    }

    /**
     * Set a new CameraPosition at the specific time position.
     * 
     * @param cam CameraPosition
     * @param pos int
     */
    public void addKey(CameraPosition cam, int pos)
    {
        kinematicPanel.addKey(pos);
    }

    public class PanelKinematics implements ActionListener, ChangeListener

    {
        // to define the panel :
        private JPanel panel = null;
        private Recorder recorder;

        /**
         * Constructor
         * @param kine Kinematics3D
         */
        public PanelKinematics(Kinematics3D kine)
        {
            kinemAnim = new KinematicsThread();
            createPanel();
        }

        /**
         * Build the panel.
         */
        private void createPanel()
        {
            panel = new JPanel();
            panel.setLayout(new BorderLayout());

            animPanel = new AnimationPanel(this, this);
            animPanel.getTimeSlider().setCameraPositions(getCameraPositions());
            panel.add(animPanel);
        }

        /**
         * Get the panel to be used in the GUI for the list of marching cube.
         * Build it on the first time.
         * 
         * @return the panel to be used
         */
        public JPanel getPanel()
        {
            if (panel != null)
                return panel;

            createPanel();
            return panel;
        }

        public void addKey(int t)
        {
            addKey(null, t);
        }

        public void addKey(CameraPosition cam, int t)
        {
            seekAndDestroyKeyAt(t);

            // Sequence s = canvas.getSequence();
            // s.setSelectedT( s.getSelectedT()+1 );

            // animPanel.getTimeSlider().addKey(t);

            if (cam == null)
                cam = flyCam.getCameraPosition();
            cam.timeIndex = t;
            // povd.CameraType = canvas.getCameraType();
            System.out.println(cam);
            getCameraPositions().add(cam);

        }

        public void actionPerformed(ActionEvent event)
        {

            String action = event.getActionCommand();

            if (action == "gotostart")
            {
                animPanel.getTimeSlider().setValue(0);
            }

            if (action == "gotoend")
            {
                animPanel.getTimeSlider().setValue(animPanel.getTimeSlider().getMaximum());
            }

            if (action == "playbackward")
            {
                // AnimPanel.getTimeSlider().setValue(
                // AnimPanel.getTimeSlider().getMaximum() );
            }

            if (action == "playforward")
            {
                if (getNumberOfUserKey() < 2)
                {
                    System.out.println("Must have at least 2 keys to create animation.");
                    // animPanel.setEnabledRecursive(animPanel, true);
                    return;
                }

                animPanel.setEnabledRecursive(animPanel, false);
                animPanel.getStop().setEnabled(true);
                animPanel.getStop().requestFocus();

                if (kinemAnim != null)
                {
                    kinemAnim.stop = true;
                    kinemAnim = new KinematicsThread();
                }
                ThreadUtil.bgRun(kinemAnim);
            }

            if (action == "stop")
            {
                if (kinemAnim != null)
                    kinemAnim.stop = true;
                animPanel.setEnabledRecursive(animPanel, true);
                animPanel.getPlayforward().requestFocus();
            }

            if (action == "previousframe")
            {
                int v = animPanel.getTimeSlider().getValue();
                v--;
                if (v < 0)
                    v = 0;
                animPanel.getTimeSlider().setValue(v);
            }

            if (action == "nextframe")
            {
                int v = animPanel.getTimeSlider().getValue();
                v++;
                if (v > animPanel.getTimeSlider().getMaximum())
                    v = animPanel.getTimeSlider().getMaximum();
                animPanel.getTimeSlider().setValue(v);
            }

            if (action == "recordCancel")
            {
                if (recorder != null)
                    recorder.recording = false;
            }

            if (action == "record")
            {
                if (recorder != null)
                {
                    recorder.recording = false;
                }
                if (getNumberOfUserKey() < 2)
                {
                    MessageDialog.showDialog("You should at least have 2 keys to start rendering.");
                    return;
                }

                final JFileChooser fc = new JFileChooser(prefs.get(LAST_DIR, ""));
                fc.setFileFilter(new FileNameExtensionFilter("AVI Files", "avi", "AVI"));
                fc.addChoosableFileFilter(new FileNameExtensionFilter("MP4 Files", "mp4", "MP4"));
                fc.addChoosableFileFilter(new FileNameExtensionFilter("MOV Files", "mov", "MOV"));
                fc.setMultiSelectionEnabled(false);
                int res = fc.showSaveDialog(Icy.getMainInterface().getMainFrame());
                if (res == JFileChooser.APPROVE_OPTION)
                {
                    File file = fc.getSelectedFile();
                    if (file == null)
                        return;
                    prefs.put(LAST_DIR, file.getParent());
                    if (FileUtil.getFileExtension(file.getAbsolutePath(), false).isEmpty())
                    {
                        String desc = fc.getFileFilter().getDescription();
                        if (desc.contentEquals("MP4 Files"))
                            file = new File(file.getAbsoluteFile() + ".mp4");
                        else if (desc.contentEquals("MOV Files"))
                            file = new File(file.getAbsoluteFile() + ".mov");
                        else
                            file = new File(file.getAbsoluteFile() + ".avi");
                    }
                    recorder = new Recorder(file);
                    ThreadUtil.bgRun(recorder);
                }
            }

            if (action == "previouskey")
            {
                CameraPosition previous = getPreviousFlyCamKey(animPanel.getTimeSlider().getValue() - 1, false);
                if (previous != null)
                {
                    animPanel.getTimeSlider().setValue(previous.timeIndex);
                    flyCam.setCameraPosition(previous, true);
                }

            }
            if (action == "nextkey")
            {
                CameraPosition next = getNextFlyCamKey(animPanel.getTimeSlider().getValue() + 1, false);
                if (next != null)
                {
                    animPanel.getTimeSlider().setValue(next.timeIndex);
                    flyCam.setCameraPosition(next, true);
                }
            }

            if (action == "removekey")
            {
                if (getNumberOfUserKey() > 1)
                {
                    int t = animPanel.getTimeSlider().getValue();
                    seekAndDestroyKeyAt(t);
                    // animPanel.getTimeSlider().removeKey(t);
                }
                else
                {
                    System.out.println("Animator > Last remaining animation's key cannot be removed.");
                }

            }

            if (action == "addkey")
            {
                // Destroy potential key at time t.
                int t = animPanel.getTimeSlider().getValue();
                // animPanel.getTimeSlider().removeKey(t); // same for time
                // slider
                addKey(t);
            }
            if (action == "resetkey")
            {
                // animPanel.getTimeSlider().removeAllKey();
                getCameraPositions().clear();

                // Ajoute une clef de chaque camera.
                // int cameratype = canvas.getCameraType();
                // canvas.setCameraType(CanvasViewPort.CAMERA_FLYCAM);
                addKey(0);
                // canvas.setCameraType(CanvasViewPort.CAMERA_ROTATION);
                // AddKey(0);
                // canvas.setCameraType(cameratype);

                animPanel.getTimeSlider().setValue(0);
                animPanel.getTimeSlider().repaint();
            }

            if (action == "rescale")
            {
                // Rescale TimeLine.

                rescaleTimeLine();

            }

            // if (action == "saveanimation") {
            // // Create a file chooser
            // JFileChooser fc = new JFileChooser();
            // int returnVal = fc.showSaveDialog(null);
            //
            // if (returnVal == JFileChooser.APPROVE_OPTION) {
            // File file = fc.getSelectedFile();
            //
            // try {
            // FileOutputStream fos;
            // fos = new FileOutputStream( file );
            // ObjectOutputStream oos = new ObjectOutputStream(fos);
            //
            // AnimationSaveData d = new AnimationSaveData();
            // d.CampovData = CamPointOfViewKey;
            // d.fps = AnimPanel.getTimeSlider().fps;
            // d.smoothfactor = AnimPanel.smoothpathfactor.getText();
            // d.timelenght = AnimPanel.getTimeSlider().getTimelength();
            // oos.writeObject( d );
            // fos.close();
            //
            // }
            // catch (FileNotFoundException e) { e.printStackTrace(); }
            // catch (IOException e) { e.printStackTrace(); }
            // }
            // }

            // if (action == "openanimation") {
            // // Create a file chooser
            // JFileChooser fc = new JFileChooser();
            // int returnVal = fc.showOpenDialog(null);
            //
            // if (returnVal == JFileChooser.APPROVE_OPTION) {
            // File file = fc.getSelectedFile();
            // try {
            //
            // FileInputStream fis = new FileInputStream( file );
            // ObjectInputStream ois = new ObjectInputStream(fis);
            // AnimationSaveData d =
            // (AnimationSaveData)ois.readObject();
            // CamPointOfViewKey = d.CampovData;
            // AnimPanel.getTimeSlider().setFps(d.fps);
            // AnimPanel.getTimeSlider().setTimelength( d.timelenght);
            // AnimPanel.getTimeSlider().setMaximum( d.fps * d.timelenght );
            // AnimPanel.rescalefps.setText( ""+d.fps );
            // AnimPanel.rescaletime.setText( ""+d.timelenght );
            // AnimPanel.smoothpathfactor.setText( "" + d.smoothfactor
            // );
            // fis.close();
            // }
            // catch (FileNotFoundException e) { e.printStackTrace(); }
            // catch (IOException e) { e.printStackTrace(); }
            // catch (ClassNotFoundException e) { e.printStackTrace(); }
            //
            // RefreshAnimPanel();
            // }
            // }

        }

        public void stateChanged(ChangeEvent e)
        {
            // Listening to Animation's Panel TimeSlider.
            if (animPanel.getCheckBoxAutoUpdate().isSelected() || (kinemAnim != null && !kinemAnim.stop))
            {
                if (getNumberOfUserKey() == 0)
                    addKey(0); // Prevent system from having no key to read at
                // all
                JSlider slider = (JSlider) e.getSource();
                CameraPosition d = new CameraPosition();
                d = computeSmoothInterpolatedCamKey(slider.getValue(),
                        Integer.parseInt(animPanel.getSmoothpathfactor().getText()));

                if (d != null)
                    flyCam.setCameraPosition(d, true);
            }

        }

    }

    public void actionPerformed(ActionEvent e)
    {

    }

    public void rescaleTimeLine()
    {
        int oldfps = animPanel.getTimeSlider().getFps();
        int oldlength = animPanel.getTimeSlider().getTimelength();
        int newfps = Integer.parseInt(animPanel.getRescalefps().getText());
        int newlength = Integer.parseInt(animPanel.getRescaletime().getText());

        animPanel.getTimeSlider().setFps(newfps);
        animPanel.getTimeSlider().setTimelength(newlength);
        // animPanel.getTimeSlider().removeAllKey();
        animPanel.getTimeSlider().setMaximum(newfps * newlength);
        animPanel.getRescalefps().setText("" + newfps);
        animPanel.getRescaletime().setText("" + newlength);

        // change towards length
        double timeratio = newlength / (double) oldlength;
        timeratio *= newfps / (double) oldfps;
        for (CameraPosition cam : getCameraPositions())
            cam.timeIndex *= timeratio;

        animPanel.getTimeSlider().setValue((int) (animPanel.getTimeSlider().getValue() * timeratio));

        // Put new computed key in Animation's Slider.
        // for (Enumeration<CameraPosition> e = cameraPositions.elements();
        // e.hasMoreElements();)
        // {
        // CameraPosition povk = e.nextElement();
        // if (canvas.getCameraType() == povk.CameraType)
        // animPanel.getTimeSlider().addKey(povk.timeIndex);
        // }
    }

    private class KinematicsThread implements Runnable
    {

        private boolean stop = false;

        /**
         * Play the animation // And has record capability
         */
        @Override
        public void run()
        {
            int idx = animPanel.getTimeSlider().getValue();
            int size = animPanel.getTimeSlider().getTimelength();
            int fps = animPanel.getTimeSlider().getFps();
            // long time = 0;

            idx = animPanel.getTimeSlider().getValue();
            size = animPanel.getTimeSlider().getTimelength() * animPanel.getTimeSlider().getFps();
            fps = animPanel.getTimeSlider().getFps();
            // time = System.nanoTime();

            while (!stop)
            {
                if (idx >= size)
                    idx = 0;

                ThreadUtil.sleep((int) (1d / fps * 1000));
                // long delay;
                // delay = (System.nanoTime() - time) * 1000000L;

                // if (animPanel.getCheckBoxSmooth().isSelected())
                // {
                // float step;
                // step = delay / (float) (1000 / fps);
                // idx += step;
                // }
                // else
                // {
                idx += 1;
                // }

                animPanel.getTimeSlider().setValue(idx);

                // time = System.nanoTime();

            }
        }
    }

    class Recorder implements Runnable
    {
        private boolean recording;
        private IRational frameRate;
        private long nextTimeStamp;
        private File f;
        private IMediaWriter writer;

        public Recorder(File f)
        {
            this.f = f;
        }

        @Override
        public void run()
        {
            VtkCanvas canvas = flyCam.getCanvas();

            if (canvas == null)
                return;

            int width = canvas.getVtkPanel().getWidth();
            int height = canvas.getVtkPanel().getHeight();
            int fps = Integer.valueOf(animPanel.getRescalefps().getText());

            frameRate = IRational.make(fps, 1);
            writer = ToolFactory.makeWriter(f.getAbsolutePath());
            writer.addVideoStream(0, 0, frameRate, width, height);

            String ext = FileUtil.getFileExtension(f.getAbsolutePath(), false).toLowerCase();
            if (ext.contentEquals("mp4") || ext.contentEquals("mov"))
            {
                IStreamCoder coder = writer.getContainer().getStream(0).getStreamCoder();
                coder.setCodec(ICodec.ID.CODEC_ID_H264);
                coder.setPixelType(IPixelFormat.Type.YUV422P);
            }

            recording = true;

            nextTimeStamp = 0;

            // final int nbframe = animPanel.getTimeSlider().getFps() *
            // animPanel.getTimeSlider().getTimelength();
            final int nbframe = animPanel.getTimeSlider().getMaximum();

            ThreadUtil.invokeLater(new Runnable()
            {
                @Override
                public void run()
                {
                    animPanel.getRecordProgressBar().setMaximum(nbframe);
                    animPanel.getRecordProgressBar().setValue(0);
                }
            });
            try
            {
                for (int i = 0; i < nbframe && recording; i++)
                {
                    final CameraPosition d = computeSmoothInterpolatedCamKey(i,
                            Integer.parseInt(animPanel.getSmoothpathfactor().getText()));
                    final int tPos = i;
                    int sequenceT = canvas.getPositionT();
                    if (animPanel.isAutoChangeSeqT())
                        sequenceT = (sequenceT + i) % canvas.getSequence().getSizeT();

                    canvas = flyCam.getCanvas();
                    if ((canvas == null) || (canvas.getRenderer() == null))
                        return;

                    // set camera position
                    flyCam.setCameraPosition(d, false);

                    BufferedImage originalImage = canvas.getRenderedImage(sequenceT, -1);
                    BufferedImage worksWithXugglerBufferedImage = ImageUtil.convert(originalImage, new BufferedImage(
                            originalImage.getWidth(), originalImage.getHeight(), BufferedImage.TYPE_3BYTE_BGR));

                    writer.encodeVideo(0, worksWithXugglerBufferedImage, nextTimeStamp, TimeUnit.MICROSECONDS);

                    nextTimeStamp += (1e6 / frameRate.getNumerator());
                    // recordedSequence.setImage(tPos, 0,
                    // canvas.getRenderedImage(sequenceT, -1));
                    ThreadUtil.invokeLater(new Runnable()
                    {
                        @Override
                        public void run()
                        {
                            animPanel.getTimeSlider().setValue(tPos);
                            animPanel.getRecordProgressBar().setValue(tPos);
                        }
                    });
                }
            }
            finally
            {
                writer.close();
                ThreadUtil.invokeLater(new Runnable()
                {
                    @Override
                    public void run()
                    {
                        animPanel.getRecordProgressBar().setValue(0);
                    }
                });
            }
        }
    }

    public void clearKeys()
    {
        getCameraPositions().clear();
    }

    public void close()
    {
        flyCam = null;
        if (kinemAnim != null)
        {
            kinemAnim.stop = true;
            kinemAnim = null;
        }
        if (animPanel != null)
        {
            animPanel.removeListeners();
            animPanel.removeAll();
            animPanel = null;
        }
    }

    public void setFlyCamOverlay(FlyCamOverlay flyCam)
    {
        this.flyCam = flyCam;
    }
}
