package plugins.tprovoost.animation3d;

import icy.gui.dialog.MessageDialog;
import icy.gui.frame.IcyFrame;
import icy.plugin.PluginLauncher;
import icy.plugin.abstract_.PluginActionable;
import icy.sequence.Sequence;
import icy.system.thread.ThreadUtil;

import java.util.ArrayList;

import plugins.tprovoost.flycam.FlyCam;
import plugins.tprovoost.flycam.FlyCamOverlay;

public class Animation3D extends PluginActionable
{
    private Kinematics3D mainFrame;

    @Override
    public void run()
    {
        Sequence seq = getActiveSequence();
        if (seq == null)
        {
            MessageDialog.showDialog("You must have a sequence opened for this operation.");
            return;
        }

        // FlyCam is not running on this sequence ?
        if (!seq.hasOverlay(FlyCamOverlay.class))
        {
            // start FlyCam
            ThreadUtil.invokeNow(new Runnable()
            {
                @Override
                public void run()
                {
                    new FlyCam().run();
                }
            });
        }

        // get the FlyCam overlay
        final FlyCamOverlay overlay = (FlyCamOverlay) seq.getOverlays(FlyCamOverlay.class).get(0);

        // create the frame now
        mainFrame = new Kinematics3D(overlay);
        mainFrame.setVisible(true);
        mainFrame.addToDesktopPane();
    }

    public Kinematics3D getKinematics3D()
    {
        return mainFrame;
    }

    /**
     * Returns the first Animation3D in the list of IcyFrames. If it doesn't
     * exist, create an instance first.
     * 
     * @return Kinematics3D
     */
    public static Kinematics3D getAnimation3D()
    {
        ArrayList<IcyFrame> frames = IcyFrame.getAllFrames(Kinematics3D.class);
        if (frames.isEmpty())
        {
            Animation3D p = (Animation3D) PluginLauncher.start(Animation3D.class.getName());
            return p.getKinematics3D();
        }
        else
            return (Kinematics3D) frames.get(0);
    }

    /**
     * Returns the Animation3D of index <code>idx</code> in the list of
     * IcyFrames. If it doesn't exist, returns null;
     * 
     * @param idx int
     * @return Kinematics3D
     */
    public static Kinematics3D getAnimation3D(int idx)
    {
        ArrayList<IcyFrame> frames = IcyFrame.getAllFrames(Kinematics3D.class);
        if (frames.isEmpty())
        {
            return null;
        }
        else
        {
            return (Kinematics3D) frames.get(idx);
        }
    }
}
